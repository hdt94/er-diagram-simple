/* database */
DROP DATABASE IF EXISTS market_tickets;
CREATE DATABASE market_tickets;
\c market_tickets;
--
--
/* basic tables*/
CREATE TABLE locations (
    location_id SERIAL PRIMARY KEY,
    address VARCHAR NOT NULL,
    city VARCHAR NOT NULL,
    descr VARCHAR,
    name VARCHAR(50) NOT NULL
);
CREATE TABLE users (
    user_id SERIAL PRIMARY KEY,
    email VARCHAR(100) NOT NULL,
    name VARCHAR(50) NOT NULL
);
--
--
/* tables with foreign keys */
CREATE TABLE events (
    event_id SERIAL PRIMARY KEY,
    descr VARCHAR,
    location_id INTEGER REFERENCES locations (location_id),
    name VARCHAR(150) NOT NULL,
    ts TIMESTAMPTZ
);
CREATE TABLE places (
    place_id SERIAL PRIMARY KEY,
    descr VARCHAR,
    location_id INTEGER REFERENCES locations (location_id),
    pos_x VARCHAR,
    pos_y VARCHAR
);
CREATE TABLE prices (
    price_id SERIAL PRIMARY KEY,
    category VARCHAR,
    event_id INTEGER REFERENCES events (event_id),
    value DOUBLE PRECISION NOT NULL
);
CREATE TABLE tickets (
    ticket_id SERIAL PRIMARY KEY,
    event_id INTEGER REFERENCES events (event_id),
    is_available BOOLEAN DEFAULT false,
    place_id INTEGER REFERENCES places (place_id),
    price_id INTEGER REFERENCES prices (price_id)
);
CREATE TABLE purchases (
    purchase_id SERIAL PRIMARY KEY,
    ticket_id INTEGER REFERENCES tickets (ticket_id),
    ts TIMESTAMPTZ DEFAULT NOW(),
    user_id INTEGER REFERENCES users (user_id)
);
