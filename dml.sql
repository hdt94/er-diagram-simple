INSERT INTO locations (address, city, descr, name)
VALUES ('St 123', 'Cali', 'awesome!', 'HT');
--
INSERT INTO users (email, name)
VALUES ('hdoe@email.tld', 'h doe');
--
INSERT INTO events (descr, location_id, name, ts)
SELECT 'networking', loc.location_id, 'NET HT', '2025-06-22 13:00'
FROM locations AS loc
LIMIT 1;
--
INSERT INTO places (descr, location_id, pos_x, pos_y)
SELECT 'lower stands', loc.location_id, 'A1', 'B2'
FROM locations AS loc
LIMIT 1;
--
INSERT INTO prices (category, event_id, value)
SELECT 'gold', ev.event_id, 10.5
FROM events AS ev
LIMIT 1;
--
WITH pla AS (
    SELECT place_id
    FROM places
    LIMIT 1
),
pri AS (
    SELECT price_id
    FROM prices
    LIMIT 1
)
INSERT INTO tickets (event_id, is_available, place_id, price_id)
SELECT ev.event_id, true, pla.place_id, pri.price_id
FROM events AS ev
CROSS JOIN pla
CROSS JOIN pri
LIMIT 1;
--
WITH u AS (
    SELECT user_id
    FROM users
    LIMIT 1
)
INSERT INTO purchases (ticket_id, ts, user_id)
SELECT ti.ticket_id, NOW(), u.user_id
FROM tickets AS ti
CROSS JOIN u
LIMIT 1;
