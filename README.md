# ER design

Minimal Entity-Relationship ER diagram exercise considering as general use case buying tickets that can be sold out.

This can be extended in many forms, for example, pricing can be also modeled referencing marketing campaigns entitiy instead of only using a category field.

## Design

Entities:
- locations: locations in city.
- events: market events.
- places: marked places within locations to be accessed through ticket purchase.
- prices: event prices based on category.
- purchases: ticket purchases by users.
- tickets: ticket for a place in an event.
- users: system users that buy tickets.

Use cases:
- User checks events in a location.
- User checks prices and available places in an event location
- User buys a ticket for a selected place in an event location.

## ER diagram

![](er-diagram.png)


## Tables

### Basic tables

locations
---
attribute | datatype | description
----------|----------|------------
location_id | ID | row identifier
address | VARCHAR | location address in city
city | VARCHAR | city name
descr | VARCHAR | location description
name | VARCHAR | location name


users
---
attribute | datatype | description
----------|----------|------------
user_id | ID | row identifier
email | VARCHAR | user email
name | VARCHAR | user name


### Tables with foreign keys

events
---
attribute | datatype | description
----------|----------|------------
event_id | ID | row identifier
descr | VARCHAR | event description
location_id | ID FOREIGN | reference to location
name | VARCHAR | event name
ts | TIMESTAMP | event date time

places
---
attribute | datatype | description
----------|----------|------------
place_id | ID | row identifier
descr | VARCHAR | place description
location_id | ID FOREIGN | reference to location
pos_x | VARCHAR | position X in location
pos_y | VARCHAR | position Y in location

prices
---
attribute | datatype | description
----------|----------|------------
price_id | ID | row identifier
category | VARCHAR | price category
event_id | ID FOREIGN | reference to event
value | DOUBLE | price value

purchases
---
attribute | datatype | description
----------|----------|------------
purchase_id | ID | row identifier
ticket_id | ID FOREIGN | reference to ticket
ts | TIMESTAMP | purchase date time
user_id | ID FOREIGN | reference to user

tickets
---
attribute | datatype | description
----------|----------|------------
ticket_id | ID | row identifier
event_id | ID FOREIGN | reference to event
is_available | BOOLEAN | availability for faster reading
place_id | ID FOREIGN | reference to place
price_id | ID FOREIGN | reference to price


## Implementation with PostgreSQL

Lift up a local PostgreSQL instance:
```sh
docker run -it --rm \
    --name pg-insecure-ephemeral \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    -p 5432:5432 \
    postgres
```

Create tables:
```sh
psql -U postgres -f ddl.sql
```

Insert data:
```sh
psql -U postgres -d market_tickets -f dml.sql
```
